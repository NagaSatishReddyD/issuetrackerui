import React from 'react';
import {  Table } from 'semantic-ui-react';

export const IssueRecords = ()=>{
    return(
        <Table textAlign="center" style={{  marginTop:"30px" }} >
            <Table.Header>
                <Table.HeaderCell style={{width:700}}>Description</Table.HeaderCell>
                <Table.HeaderCell>Status</Table.HeaderCell>
                <Table.HeaderCell>Last Modified</Table.HeaderCell>
                <Table.HeaderCell>Submitted Date</Table.HeaderCell>
            </Table.Header>
            <Table.Body>
                <Table.Row>
                <Table.Cell>Some Description </Table.Cell>
                <Table.Cell>Submitted</Table.Cell>
                <Table.Cell>25/03/2021</Table.Cell>
                <Table.Cell>31/03/2021</Table.Cell>
                </Table.Row>
            </Table.Body>
        </Table>
    );
};