import "semantic-ui-css/semantic.min.css";
import "./App.css";
import React from "react";
import { Route, Switch } from "react-router-dom";
import SignIn from "./login/SignIn";
import { Container } from "semantic-ui-react";
import { SetPassword } from "./login/SetPassword";
import {IssueRecords} from "./issues/IssueRecords";

function App() {
  return (
    <Switch>
      <Container>
        {/* <Route
          path="/"
          render={() => {
            return <Redirect to="/login" />;
          }}
        /> */}
        {/* <Redirect exact path="/" to="login"/> */}

        <Route exact path="/login" component={SignIn} />
        <Route exact path="/setpassword" component={SetPassword} />
        <Route exact path='/issues'>
          <IssueRecords/>
        </Route>
      </Container>
    </Switch>
  );
}

export default App;
