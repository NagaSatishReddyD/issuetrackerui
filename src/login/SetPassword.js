import React from "react";
import { Button, Form, Grid, Header, Segment } from "semantic-ui-react";

export const SetPassword = () => {
  return (
    <Grid textAlign="center" style={{ height: "100vh" }} verticalAlign="middle">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as="h2" color="teal" textAlign="center">
          SetUp Your Password
        </Header>
        <Form size="large">
          <Segment stacked>
            <Form.Input fluid placeholder="New Password" type="password" />
            <Form.Input fluid placeholder="Re-type Password" type="password" />
            <Button color="teal" fluid size="large" type="submit">
              Set Password
            </Button>
          </Segment>
        </Form>
      </Grid.Column>
    </Grid>
  );
};
